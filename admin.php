<?php
  //ini_set('error_reporting', E_ALL);
  //ini_set('display_errors', 1);
  //ini_set('display_startup_errors', 1);

  session_start();
    
  if( ($_SESSION['role']) == 'owner' )
  { 
      $upLoadDir = "tests/";
      if (!is_dir($upLoadDir)){
         mkdir($upLoadDir, 0700);
      }
      
      $upLoadFile = $upLoadDir . basename($_FILES['userfile']['name']);
      if (move_uploaded_file($_FILES['userfile']['tmp_name'], $upLoadFile))
      {
        $ref = "Location: list.php";   
        header($ref);
        exit;
      }
  }
  else
  {   
      header($_SERVER['SERVER_PROTOCOL'] . ' 403 Unautorized');
      exit('<h1>403 Unautorized</h1><p>Перейти к <a href="index.php">форме авторизации</a></p>');   
  }
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta charset="utf-8">
      <title>Загрузка теста</title>
      <link rel="stylesheet" href="style.css">
  </head>
  <body>
     <h2>Загрузка теста </h2>
     <form enctype="multipart/form-data" action="" method="POST">
         <br>
         <input name="userfile" type="file" />
         <br>
         <input type="submit" value="Загрузить">
     </form>
     <hr>

  </body>
</html>
