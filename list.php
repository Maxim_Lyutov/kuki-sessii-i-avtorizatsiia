<?php
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    session_start();
    
	if( (session_id() == "") || empty($_SESSION['user']) )
	{
	   header($_SERVER['SERVER_PROTOCOL'] . ' 403 Unautorized');
	   exit('<h1>403 Unautorized</h1><p>Перейти к <a href="index.php">форме авторизации</a></p>');
	}
	
    if ( (isset($_GET['idTestDel'])) && ( $_SESSION['role'] == 'owner') ) 
    {
        unlink($_GET['idTestDel']);
    }

    $fileList = glob("tests/*.json");

    function getNameTest($nameFile)
    {
      $json = file_get_contents($nameFile, FILE_USE_INCLUDE_PATH);
      $arr = json_decode($json, true);
      return $arr['name'] ;
    }

    function getNomerTest($nameFile)
    {
      $json = file_get_contents($nameFile, FILE_USE_INCLUDE_PATH);
      $arr = json_decode($json, true);
      return $arr['nomer'] ;
    }

?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta content="text/html; charset=utf-8" />
      <title>Задание : Куки ,Сессия </title>
      <link rel="stylesheet" href="style.css">
  </head>
  <body>
     
     <?php  if( $_SESSION['role'] == 'owner' ): ?>
       <p>Ваш логин: <?= $_SESSION['user'] ?> <a href='index.php'> Выход </a> </p>
       <p><a href='admin.php'> Добавить Тест </a></p>       
     <?php endif; ?>
     
     <?php  if( $_SESSION['role'] != 'owner' ): ?>
        <p> Приветствую тебя, <span class='user'><?=$_SESSION['user']?></span> <a href='index.php'> Выход </a> </p>
     <?php endif; ?>
     
     <h3>Список тестов</h3>
     
     <table border = "1" width ="50%" >
        <tr>
           <td> # </td>
           <td> Название теста </td>
           <td> Файл </td>
           <td> Переход на тест </td>
        
           <?php  if( $_SESSION['role'] == 'owner' ): ?>
               <td> Удалить тест </td>
           <?php endif; ?>
           
        </tr>
      <?php foreach ($fileList as $ind => $file) : ?>
        <tr>
            <td> <?=$ind+1?> </td>
            <td> <?php echo getNameTest($file) ?> </td>
            <td> <?php echo $file ?> </td>
            <td> <a href = "/test.php?idTest=<?php echo getNomerTest($file) ?> "> Начать </a></td>
       <?php  if( $_SESSION['role'] == 'owner' ): ?>
              <td> <a href = "/list.php?idTestDel=<?=$file?>"> Удалить </a></td>
       <?php endif; ?>
         </tr>
     <?php endforeach; ?>
     </table>
     
  </body>
</html>
