<?php
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    
    $errText = null;
    session_start();
	
    if ($_POST)
    {
        if ( isset($_POST['guest']) ) // это Гость ?
        {
           $_SESSION['user'] = $_POST['guest'];
           $_SESSION['role'] = 'guest'; 
           header('Location: list.php');
           exit;
        }
        elseif ( file_exists($_POST['login'].'.json' ) ) // этот есть логин ? (Аутентификация)
        {
            list($pass , $role) = getParamFile($_POST['login'].'.json');
          
            if( $_POST['pass'] == $pass ) 
            { 
                $_SESSION['user'] = $_POST['login'];
                $_SESSION['pass'] = $_POST['pass'];
                $_SESSION['role'] = $role; // доступ к функционалу (авторизирация): 'owner', 'user', 'guest'
                
                header('Location: list.php');
                exit;
            }
            else
            {
                $errText ='(2)Не верные параметры входа!'; 
            }
        }
        else
        {
            $errText ='(1)Не верные параметры входа!'; 
        }
    }
    else // для выхода пользователя из приложения
    {
        $_SESSION = array();
        if ( session_id() != "" || isset($_COOKIE[session_name()]) )
        {
            setcookie(session_name(), '', time()-2592000, '/');
            session_destroy();
        }
    }
    
	function getParamFile($nameFile)
    {
      $json = file_get_contents($nameFile, FILE_USE_INCLUDE_PATH);
      $arr = json_decode($json, true);
	  
      return array( $arr['pass'], $arr['role']);
    }
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta content="text/html; charset=utf-8" />
      <title>Форма входа</title>
      <link rel="stylesheet" href="style.css">
  </head>
  <body>
     <h2><?=$errText?></h2>
     <h3>Авторизация : </h3>
     <form action="" method="POST">
          <p>Логин <input  name="login" type="text" value="" required placeholder="..." /></p>
          <p>Пароль<input  name="pass" type="password" value="" required placeholder="..." /></p>
         <input type="submit" value="Авторизация">
     </form>
     
     <br>
     
     <h3>Вход как гость: </h3>
     <form action="" method="POST">
          <p>Имя <input  name="guest" type="text" value="" required placeholder="..." /></p>
         <input type="submit" value="Вход как гость">
     </form>
  </body>
</html>
